###介绍
一个activemq python版stomp协议的实现

1. 支持failover
2. 支持topic
3. 支持queue
4. 支持持久订阅topic
###依赖
只是对python版本有依赖，需要python>=2.7
###设计思想

采用两个线程工作

1. mqclient线程是处理从activemq接受消息和发送消息
2. mqkeeper为守护线程，时刻检查mqclient线程健康状况，如果出现和host断开情况，会从待选host中找出可用host,让mqclient线程从新链接上host

###使用
    host_list = []
    for host in mqconfig.ACTIVEMQ_HOST.split(","):
        host_list.append((host.strip(),mqconfig.ACTIVEMQ_STOMP_PORT))
    #默认链接第一个host
    mqclient = MQClient(host_list[0][0],host_list[0][1],mqconfig.USER_NAME,mqconfig.PASSWORD,mqconfig.CLIENT_ID)
    mqclient.subscribe('/topic/'+mqconfig.SVN_CI_DATA_TOPIC,9875,call_back,mqconfig.SUBSCRIBE_NAME)
    mqclient.start()
    #give sometime to mqclient to init connection
    time.sleep(10)
    keeper = MQKeeper(mqclient,host_list,retry_times=10)
    keeper.start()

如上面需要配置一个host列表，配置请见config.py
