#coding:utf-8
'''
Created on 2013-4-12

@author: Wong

'''
########################ActiveMQ##################################
ACTIVEMQ_HOST = '127.0.0.1,127.0.0.1'
ACTIVEMQ_STOMP_PORT =61613
SVN_CI_DATA_TOPIC = 'basedata.publictopic.SVNCOMMITDATA'
USER_NAME = 'system'
PASSWORD = 'manager' 
CLIENT_ID = 'cooder-binder'
SUBSCRIBE_NAME='cooder-subscriber'
