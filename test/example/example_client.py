'''
Created on 2013-4-11

@author: Wong
'''
from com.du.mq.client import MQClient
import os
def call_back(body,header):
    print body
    for key in header:
        print 'key:%s,value:%s'%(key,header[key])
if __name__ == '__main__':
    client = MQClient('localhost',61613,'system','manager','imtai')
    client.subscribe('/topic/basedata.publictopic.SVNCOMMITDATA', call_back,'mactai')
    client.start()
    pass