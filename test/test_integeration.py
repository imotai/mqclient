#encoding:utf-8
'''
Created on 2013-7-31
@author:innerp
'''
from com.du.mq import mqconfig
from com.du.mq.client import MQClient
from com.du.mq.keeper import MQKeeper
import time

def call_back(body,header):
    print body
def test_connect_broken():
    host_list = []
    for host in mqconfig.ACTIVEMQ_HOST.split(","):
        host_list.append((host.strip(),mqconfig.ACTIVEMQ_STOMP_PORT))
    #默认链接第一个host
    mqclient = MQClient(host_list[0][0],host_list[0][1],mqconfig.USER_NAME,mqconfig.PASSWORD,mqconfig.CLIENT_ID)
    mqclient.subscribe('/topic/'+mqconfig.SVN_CI_DATA_TOPIC,9875,call_back,mqconfig.SUBSCRIBE_NAME)
    mqclient.start()
    #give sometime to mqclient to init connection
    time.sleep(10)
    keeper = MQKeeper(mqclient,host_list,retry_times=10)
    keeper.start()

if __name__=="__main__":
    test_connect_broken()